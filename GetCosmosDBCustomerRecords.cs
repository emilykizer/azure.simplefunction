using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos;
using System.Linq;
using CosmosDB.Library;

namespace Azure.SimpleFunction
{
    public static class GetCosmosDBCustomerRecords
    {
        [FunctionName("GetCosmosDBCustomerRecords")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string customerId = req.Query["Id"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            customerId ??= data?.Id;

            string responseMessage = "";

            if (string.IsNullOrEmpty(customerId))
            {
                responseMessage = "Please provide a valid customer Id.";
            }
            else
            {
                var customer = new Customer();

                try
                {

                    using (var client = new CosmosClient("AccountEndpoint=https://ekizer-mindcept-cosmo.documents.azure.com:443/;AccountKey=CSsskkJIMyCa2r2NNpuGRMhd78dmPj45IVJrzin4t27dh0osY7mSMfqf0dwCR7WEbP9QMMEm7FcMpHvytM2bKA==;"))
                    {
                        var DB = client.GetDatabase("ChampionProductCosmoDB");
                        var table = client.GetContainer(DB.Id, "Customer");

                        var sqlString = "Select * from src where src.customerId = " + customerId;
                        var query = new QueryDefinition(sqlString);
                        FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                        FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                        customer = queryResult.FirstOrDefault();

                        responseMessage = JsonConvert.SerializeObject(customer);
                    };

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                    throw;
                }

            }
            return new OkObjectResult(responseMessage);
        }
    }
}
